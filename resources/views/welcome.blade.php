<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="/plugin/datatables.css">
    <link rel="stylesheet" href="/plugin/style.css">
    <title>Movie List</title>
  </head>
  <body>
    <div class="container p-3">
      <!-- Content here -->
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">Movie List</li>
        </ol>
      </nav>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-12">
          <button class="btn btn-info" type="button" id="btnAddMovie" data-toggle="modal" data-target="#addedMovie">Movie Added</button>
        </div>
      </div>
      <table class="table text-center" id="tableListMovie">
        <thead>
          <tr>
            <th scope="col">id</th>
            <th scope="col">No</th>
            <th scope="col">Title</th>
            <th scope="col">Date</th>
            <th scope="col">Rating</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>      
    </div>

    {{-- Modal Added Movie --}}
    <div class="modal fade" id="addedMovie" tabindex="-1" aria-labelledby="addedMovieLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="addedMovieLabel">Modal Added Movie</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-12">
                <div class="form-group">
                  <label for="">Movie Title</label>
                  <input type="text" class="form-control" placeholder="Movie Title" id="val-title-movie">
                </div>
              </div>
              <div class="col-12">
                <div class="form-group">
                  <label for="">Movie Date Released</label>
                  <input type="date" class="form-control" placeholder="Movie Date Released" id="val-date-movie">
                </div>
              </div>
              <div class="col-12">
                <div class="form-group">
                  <label for="">Movie Rating</label>
                  <div class="range-wrap">
                    <div class="range-value" id="rangeV"></div>
                    <input type="range" min="0" max="10" value="0" step="1" id="val-range-movie">
                  </div>
                </div>
              </div>
              <div class="col-12">
                <div class="form-group">
                  <label for="">Movie Description</label>
                  <textarea name="" id="textarea-description-add" cols="30" rows="10"></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btnSaveMovie" >Save Movie</button>
          </div>
        </div>
      </div>
    </div>

    
    {{-- Modal Update Movie --}}
    <div class="modal fade" id="updateMovie" tabindex="-1" aria-labelledby="updateMovieLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="updateMovieLabel">Modal Update Movie</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <input type="hidden" id="update-id-movie">
            <div class="row">
              <div class="col-12">
                <div class="form-group">
                  <label for="">Movie Title</label>
                  <input type="text" class="form-control" placeholder="Movie Title" id="update-title-movie">
                </div>
              </div>
              <div class="col-12">
                <div class="form-group">
                  <label for="">Movie Date Released</label>
                  <input type="date" class="form-control" placeholder="Movie Date Released" id="update-date-movie">
                </div>
              </div>
              <div class="col-12">
                <div class="form-group">
                  <label for="">Movie Rating</label>
                  <div class="range-wrap">
                    <div class="range-value" id="update-rangeV"></div>
                    <input type="range" min="0" max="10" value="0" step="1" id="update-range-movie">
                  </div>
                </div>
              </div>
              <div class="col-12">
                <div class="form-group">
                  <label for="">Movie Description</label>
                  <textarea  id="textarea-description-update" cols="30" rows="10"></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btnUpdateMovie" >Update Movie</button>
          </div>
        </div>
      </div>
    </div>

    
    {{-- Modal delete Movie --}}
    <div class="modal fade" id="deleteMovie" tabindex="-1" aria-labelledby="deleteMovieLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="deleteMovieLabel">Modal Delete Movie</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <input type="hidden" id="delete-id-movie">
            <div class="row">
              <div class="col-12">
                <div class="form-group">
                  <label for="">Movie Title</label>
                  <input type="text" class="form-control" placeholder="Movie Title" readonly id="delete-title-movie">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger" id="btnDeleteMovie" >Delete Movie</button>
          </div>
        </div>
      </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script
  src="https://code.jquery.com/jquery-3.5.1.js"
  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
  crossorigin="anonymous"></script>
    {{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="/tinymce/js/tinymce/tinymce.min.js"></script>
    <script src="/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="/plugin/datatables.js"></script>
    <script src="/plugin/script.js"></script>
    
  </body>
</html>