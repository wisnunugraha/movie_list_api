-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.6-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.6002
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for movie_db
CREATE DATABASE IF NOT EXISTS `movie_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `movie_db`;

-- Dumping structure for table movie_db.movie_list
CREATE TABLE IF NOT EXISTS `movie_list` (
  `movie_id` varchar(50) NOT NULL,
  `movie_name` varchar(50) DEFAULT NULL,
  `movie_date` varchar(50) DEFAULT NULL,
  `movie_rating` varchar(50) DEFAULT NULL,
  `movie_describe` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`movie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table movie_db.movie_list: ~0 rows (approximately)
/*!40000 ALTER TABLE `movie_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `movie_list` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
