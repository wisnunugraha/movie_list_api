<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/', function () {
  return 'Hayo mau ngapain...';
});

// Datatable here
Route::get('/get-data-movie','Api\MovieController@GetDataMovie');
// Post Data Here
Route::post('/post-data-movie','Api\MovieController@PostDataMovie');
// Get Data By ID
Route::get('/get-data-by-id/{id}','Api\MovieController@GetDataByID');
// Update Data
Route::post('/update-data-movie','Api\MovieController@UpdateDataMovie');
// Delete Data
Route::get('/delete-data-movie','Api\MovieController@DeleteDataMovie');




// Route Fallback if Route doesn't exist
Route::fallback(function(){
  return response()->json([
      'status'    => false,
      'message'   => 'Page Not Found.',
  ]);
});