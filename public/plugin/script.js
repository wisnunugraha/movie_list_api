
      $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var table = $('#tableListMovie').DataTable({

                      "processing": true,
                      "bDestroy": true,
                      "bInfo": true,
                      "bSortable": false,
                      "bPaginate": true,
                      "bLengthChange": false,
                      "retrieve": true, 
                      // "bFilter": false, 
                      "order":[1,"asc"],
                      ajax: {
                        url: '/api/get-data-movie',
                        type: 'get',
                        method: 'get',
                        dataSrc : '',
                      },
                      'columns': [
                        {'data' : 'id'},
                        {'data' : 'no'},
                        {'data' : 'name'},
                        {'data' : 'tanggal'},
                        {'data' : 'rating'},

                      ],
                      'columnDefs' : [
                        {
                          "targets": 0,
                          "visible": false,
                          // "width": "20%" ,
                        },
                        {
                          "targets": 5,
                          class:"text-center",
                          data: 'id',
                          render:function(data,type,row,meta){
                              return '<button class="btn btn-success waves-effect waves-light " id="btnUpdate" data-toggle="modal" data-target="#updateMovie" > Update</button> '+
                                      ' <button class="btn btn-danger waves-effect waves-light " id="btnDelete" data-toggle="modal" data-target="#deleteMovie" > Delete</button>';;
                          }
                        },

                      ],

                    });

        $('#tableListMovie tbody').on('click','#btnUpdate', function (){
          $('#update-id-movie').val('');
          $('#update-title-movie').val('');
          $('#update-date-movie').val('');
          $('#update-range-movie').val(0);
          $('#update-rangeV').css('left','calc(0% + 10px)');
          $('#update-rangeV').find('span').html(0);
          tinymce.get('textarea-description-update').setContent('');

          const data = table.row($(this).parents('tr')).data();
          $('#update-id-movie').val(data.id);
          var title = $('#update-title-movie').val(data.name);
          var range = $('#update-range-movie').val(data.rating);
          $('#update-rangeV').css('left','calc('+data.rating+'0% + 0px)');
          $('#update-rangeV').find('span').html(data.rating);

          $.ajax({
            url: '/api/get-data-by-id/'+data.id,
            type: 'get',
            method: 'get',
            success: function (result) 
            {
              $('#update-date-movie').val(result.data[0].tanggal);
              tinymce.get('textarea-description-update').setContent(result.data[0].descriptions);
            }
          })
          

        });
        
        // Update Movie Here
        $('#btnUpdateMovie').on('click',function() {
          var id = $('#update-id-movie').val();
          var title = $('#update-title-movie').val();
          var date = $('#update-date-movie').val();
          var range = $('#update-range-movie').val();
          var desc = tinymce.get('textarea-description-update').getContent();
          if(title == '' || date == '' || range == 0)
          {
            swal("Error!", "Filled Cannot be empty!", "error");
          }
          else
          {
            $.ajax({
              url: '/api/update-data-movie',
              method: 'POST',
              type: 'POST',
              data: {'id':id,'title':title, 'date':date, 'range':range, 'desc':desc},
              success: function (result)
              {
                if(result.kode == '00')
                {
                  swal("Error!", result.message , "error");
                }
                if(result.kode == '01')
                {
                  swal("Error!", result.message , "error");
                  
                }
                if(result.kode == '02')
                {
                  $('#updateMovie').modal('hide');
                  swal("Success!", result.message , "success");
                  table.ajax.reload();
                  
                }
                if(result.kode == '03')
                {
                  swal("Error!", result.message , "error");                  
                }
              }
            })

          }
        });


        // Delete Movie Here
        
        $('#tableListMovie tbody').on('click','#btnDelete', function (){
          $('#delete-id-movie').val('');
          $('#delete-title-movie').val('');

          const data = table.row($(this).parents('tr')).data();
          $('#delete-id-movie').val(data.id);
          $('#delete-title-movie').val(data.name);          

        });

        $('#btnDeleteMovie').on('click', function (){          
          const id = $('#delete-id-movie').val();
          $('#delete-title-movie').val();
          $.ajax({
            url: '/api/delete-data-movie',
            method: 'get',
            type: 'get',
            data: {'id': id},
            success: function (result)
            {
              if(result.kode == '00')
              {
                swal("Error!", result.message , "error");
              }
              if(result.kode == '01')
              {
                swal("Error!", result.message , "error");
                
              }
              if(result.kode == '02')
              {
                $('#deleteMovie').modal('hide');
                swal("Success!", result.message , "success");
                table.ajax.reload();
                
              }
              if(result.kode == '03')
              {
                swal("Error!", result.message , "error");                  
              }
            }
          })
        });





        

        
        // Add New Movie Here
        $('#btnAddMovie').click(function(){
          
          $('#val-title-movie').val('');
          $('#val-date-movie').val('');
          $('#val-range-movie').val('0');
          $('#rangeV').css('left','calc(0% + 10px)');
          $('#rangeV').find('span').html('0');
          
        })
        
        const
        range = document.getElementById('val-range-movie'),
        rangeV = document.getElementById('rangeV'),
        setValue = ()=>{
            const
                newValue = Number( (range.value - range.min) * 100 / (range.max - range.min) ),
                newPosition = 10 - (newValue * 0.2);
            rangeV.innerHTML = `<span>${range.value}</span>`;
            rangeV.style.left = `calc(${newValue}% + (${newPosition}px))`;
        };
        document.addEventListener("DOMContentLoaded", setValue);
        range.addEventListener('input', setValue);
        
        $('#btnSaveMovie').on('click', function(){
          // alert('test')
          var title = $('#val-title-movie').val();
          var date = $('#val-date-movie').val();
          var range = $('#val-range-movie').val();
          var desc = tinymce.get('textarea-description-add').getContent();
          if(title == '' || date == '' || range == 0)
          {
            swal("Error!", "Filled Cannot be empty!", "error");
          }
          else
          {
            $.ajax({
              url:'/api/post-data-movie',
              method: 'POST',
              type : 'POST',
              data: {'title':title, 'date':date, 'range':range, 'desc':desc},
              success: function (result) 
              {
                if(result.kode == '00')
                {
                  swal("Error!", result.message , "error");
                }
                if(result.kode == '01')
                {
                  swal("Error!", result.message , "error");
                  
                }
                if(result.kode == '02')
                {
                  $('#addedMovie').modal('hide');
                  swal("Success!", result.message , "success");
                  table.ajax.reload();                  
                }
                if(result.kode == '03')
                {
                  swal("Error!", result.message , "error");                  
                }
              }
            });
          }
        });
      });

      
    
    tinymce.init({
        selector: '#textarea-description-add',
        height: 300,
        theme: 'modern',
        // readonly : 1,
        skin:'lightgray',
        plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
        image_advtab: true,
        file_picker_callback: function(callback, value, meta) {
        if (meta.filetype == 'image') {
            $('#upload').trigger('click');
            $('#upload').on('change', function() {
            var file = this.files[0];
            var reader = new FileReader();
            reader.onload = function(e) {
                callback(e.target.result, {
                alt: ''
                });
            };
            reader.readAsDataURL(file);
            });
        }
        },
        imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions',
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        // content_css: [
        //     '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        //     '//www.tinymce.com/css/codepen.min.css'
        // ]
    });
    
 
  
  
    tinymce.init({
        selector: '#textarea-description-update',
        height: 300,
        theme: 'modern',
        // readonly : 1,
        skin:'lightgray',
        plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
        image_advtab: true,
        file_picker_callback: function(callback, value, meta) {
        if (meta.filetype == 'image') {
            $('#upload').trigger('click');
            $('#upload').on('change', function() {
            var file = this.files[0];
            var reader = new FileReader();
            reader.onload = function(e) {
                callback(e.target.result, {
                alt: ''
                });
            };
            reader.readAsDataURL(file);
            });
        }
        },
        imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions',
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        // content_css: [
        //     '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        //     '//www.tinymce.com/css/codepen.min.css'
        // ]
    });
    
 

  
    const
      range = document.getElementById('update-range-movie'),
      rangeV = document.getElementById('update-rangeV'),
      setValue = ()=>{
          const
              newValue = Number( (range.value - range.min) * 100 / (range.max - range.min) ),
              newPosition = 10 - (newValue * 0.2);
          rangeV.innerHTML = `<span>${range.value}</span>`;
          rangeV.style.left = `calc(${newValue}% + (${newPosition}px))`;
      };
      document.addEventListener("DOMContentLoaded", setValue);
      range.addEventListener('input', setValue);
 