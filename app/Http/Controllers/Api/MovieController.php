<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use DB;
use Str;

class MovieController extends Controller
{
  public function GetDataMovie()
  {
    $result = array();
    $data = DB::table('movie_list')
              ->orderBy('created_at', 'asc')
              ->get();
    $no = 1;
    foreach ($data as $row)
    {
      $columns['id'] = (string) $row->movie_id;
      $columns['no'] = (string) $no++;
      $columns['name'] = (string) $row->movie_name;
      $columns['tanggal'] = (string) Carbon::parse($row->movie_date)->format('d/m/Y');
      $columns['rating'] = (string) $row->movie_rating;
      $result[] = $columns;
    }

    return response()->json($result);
  }

  public function GetDataByID($id)
  {
    // dd($id);
    if(empty($id))
    {
      $response = ['status'=>false,'message'=>'Data ID boleh kosong', 'kode' => '00'];
      return $response;
    }
    $data = DB::table('movie_list')
              ->where('movie_id', $id)
              ->select('movie_id')
              ->first();
    if(empty($data)) 
    {
      $response = ['status'=>false,'message'=>'Data tidak ditemukan', 'kode' => '01'];
      return $response;
    }
    else
    {
      $array= array();
      $result = DB::table('movie_list')
                  ->where('movie_id', $id)
                  ->first();
      
      $columns['id'] = (string) $result->movie_id;
      $columns['name'] = (string) $result->movie_name;
      $columns['tanggal'] = (string) $result->movie_date;
      $columns['rating'] = (string) $result->movie_rating;
      $columns['descriptions'] = (string) $result->movie_describe;
      $array[] = $columns;
      $response = ['status'=>true,'message'=>'Data ditemukan', 'kode' => '02', 'data'=>$array];
      return $response;
    }
  }


  public function PostDataMovie(Request $request)
  {
    // dd($request->all());
    $id = Uuid::uuid1();

    if($request->title == '' || $request->date == '' || $request->range == 0)
    {
      $response = ['status'=>false,'message'=>'Data tidak boleh kosong', 'kode' => '00'];
      return $response;
    }

    $data = DB::table('movie_list')
              ->where('movie_name', $request->title)
              ->select('movie_name')
              ->first();
    if ($data) 
    {
      $response = ['status'=>false,'message'=>'Data tidak boleh sama / data sudah ada', 'kode' => '01'];
      return $response;
    }
    else
    {
      DB::beginTransaction();
      try {
          DB::table('movie_list')
            ->insert([
              'movie_id' => $id,
              'movie_name' => $request->title,
              'movie_date' => $request->date,
              'movie_rating' => $request->range,
              'movie_describe' => $request->desc,
              'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

          DB::commit();
          $response = ['status'=>true,'message'=>'Data berhasil disimpan', 'kode' => '02'];
          return $response;
          // all good
      } catch (\Exception $error) {
          DB::rollback();
          // something went wrong
          $response = ['status'=>false,'message'=>'Data gagal disimpan'.$error , 'kode' => '03'];
          return $response;
      }
    }
  }

  public function UpdateDataMovie(Request $request)
  {
    if($request->id == '' || $request->title == '' || $request->date == '' || $request->range == 0)
    {
      $response = ['status'=>false,'message'=>'Data tidak boleh kosong', 'kode' => '00'];
      return $response;
    }
    
    DB::beginTransaction();
    try {
        DB::table('movie_list')
          ->where('movie_id',$request->id)
          ->update([
            'movie_name' => $request->title,
            'movie_date' => $request->date,
            'movie_rating' => $request->range,
            'movie_describe' => $request->desc,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
          ]);

        DB::commit();
        $response = ['status'=>true,'message'=>'Data '.$request->title.' berhasil diupdate', 'kode' => '02'];
        return $response;
        // all good
    } catch (\Exception $error) {
        DB::rollback();
        // something went wrong
        $response = ['status'=>false,'message'=>'Data gagal diupdtae'.$error , 'kode' => '03'];
        return $response;
    }

    
  }

  public function DeleteDataMovie(Request $request)
  {
    if($request->id == '' || $request->id == null)
    {
      $response = ['status'=>false,'message'=>'Data tidak boleh kosong', 'kode' => '00'];
      return $response;
    }
    
    $data = DB::table('movie_list')
              ->where('movie_id', $request->id)
              ->select('movie_id')
              ->first();
    if (empty($data)) 
    {
      $response = ['status'=>false,'message'=>'Data tidak ditemukan', 'kode' => '01'];
      return $response;
    }
    else
    {
      DB::beginTransaction();
      try {
          DB::table('movie_list')
            ->where('movie_id',$request->id)
            ->delete();
  
          DB::commit();
          $response = ['status'=>true,'message'=>'Data  berhasil dihapus', 'kode' => '02'];
          return $response;
          // all good
      } catch (\Exception $error) {
          DB::rollback();
          // something went wrong
          $response = ['status'=>false,'message'=>'Data gagal dihapus '.$error , 'kode' => '03'];
          return $response;
      }

    }

  }
}
